### Hi there 👋
---
## My name is Juan Camilo Sánchez Urrego
<img src="https://github-readme-stats.vercel.app/api?username=juancsucoder&theme=blue-green&show_icons=true" align="right"/>
I'm a self-taught programmer from Colombia, that is fascinated with everything related to computer science. I have learned a lot from the internet, and developed some interesting projects that I'll be updating on here.

![Arch Linux](https://img.shields.io/badge/I_use_Arch_Linux_btw-1793D1?style=for-the-badge&logo=arch-linux&logoColor=white)

My [Website](https://juancsucoder.github.io/), [Platzi Profile](https://platzi.com/@juancsucoder/) and [Keybase ID](https://keybase.io/juancsucoder/)

## Projects Links
- [SystemicWorks](https://juancsucoder.github.io/SystemicWorks/)

## Skills
<img src="https://github-readme-stats.vercel.app/api/top-langs/?username=JuanCSUCoder&layout=compact&theme=blue-green&langs_count=8" align="right"/>

![Python](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)
![HTML 5](https://img.shields.io/badge/HTML_5-e34c26?style=for-the-badge&logo=html5&logoColor=white)
![CSS 3](https://img.shields.io/badge/CSS-264de4?&style=for-the-badge&logo=css3&logoColor=white)
![JS](https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black)
![TS](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white)
![Express JS](https://img.shields.io/badge/Express.js-404D59?style=for-the-badge)
![React JS](https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB)

## Learning
![Rust](https://img.shields.io/badge/Rust-000000?style=for-the-badge&logo=rust&logoColor=white)
![Go](https://img.shields.io/badge/Go-00ADD8?style=for-the-badge&logo=go&logoColor=white)
![Tailwind CSS](https://img.shields.io/badge/Tailwind_CSS-38B2AC?style=for-the-badge&logo=tailwind-css&logoColor=white)

## Contact

[![Telegram @juancsucoder](https://img.shields.io/badge/Telegram_@juancsucoder-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/juancsucoder)
[![Instagram @juancsucoder](https://img.shields.io/badge/Instagram_@juancsucoder-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/juancsucoder/)
[![Twitter @juancsucoder](https://img.shields.io/badge/Twitter_@juancsucoder-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/juancsucoder)
[![LinkedIn /juancsucoder](https://img.shields.io/badge/LinkedIn_/juancsucoder-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/juancsucoder/)
[![Email jcsu2019369@gmail.com](https://img.shields.io/badge/Gmail_jcsu2019369@gmail.com-D14836?style=for-the-badge&logo=gmail&logoColor=white)](mailto:jcsu2019369@gmail.com)
[![Platzi @juancsucoder](https://img.shields.io/badge/Platzi_@juancsucoder-98ca3f?style=for-the-badge&logo=platzi&logoColor=white)](https://platzi.com/@juancsucoder/)


## Platzi Certificates
- [Fundamentals of Software Engineering](https://platzi.com/@juancsucoder/curso/1098-ingenieria/diploma/detalle/)
- [Internet Networks](https://platzi.com/@juancsucoder/curso/1277-redes/diploma/detalle/)
- [Fundamentals of Docker](https://platzi.com/@juancsucoder/curso/1432-docker/diploma/detalle/)
- [React Router](https://platzi.com/@juancsucoder/curso/1342-react-router/diploma/detalle/)
- [Kubernetes "K8s"](https://platzi.com/@juancsucoder/curso/1565-k8s/diploma/detalle/)

---

## 💬 Ask me about ... **Everything**

## My Daily.Dev
<a href="https://app.daily.dev/JuanCSUCoder"><img src="https://api.daily.dev/devcards/5b4b8101a73746909ada91500b48b5c5.png?r=eg6" width="300" alt="Juan Camilo Sánchez Urrego's Dev Card"/></a>

## 📫 How to reach me: ...
  - Telegram: [Juan Camilo Sánchez Urrego - @juancsucoder](https://t.me/juancsucoder)
  - Instagram: [Juan Camilo Sánchez Urrego - @juancsucoder](https://www.instagram.com/juancsucoder/)
  - Twitter: [Juan Camilo Sánchez Urrego - @juancsucoder](https://twitter.com/juancsucoder)
  - LinkedIn: [Juan Camilo Sánchez Urrego - /juancsucoder](https://www.linkedin.com/in/juancsucoder/)
  - Email: [Juan Camilo Sánchez Urrego - jcsu2019369@gmail.com](mailto:jcsu2019369@gmail.com)
  - Platzi: [Juan Camilo Sánchez Urrego - @juancsucoder](https://platzi.com/@juancsucoder/)
